#include <iostream>
#include <cmath>

const int ERROR_NUM = 8200;

struct result
{
    int _rslt = 0;
    bool _valid = true;
    result(const int rslt = 0, const bool valid = true) : _rslt(rslt), _valid(valid) {}
};


result add(const int a, const int b) {
    if (a == ERROR_NUM || b == ERROR_NUM || a + b == ERROR_NUM) return result(ERROR_NUM, false);
    else return result(a + b, true);
}

result multiply(int a, int b) {
    if (a == ERROR_NUM || b == ERROR_NUM || a * b == ERROR_NUM) return result(ERROR_NUM, false);

    result sum_res;
    for (int i = 0; i < b; i++) {
        sum_res = add(sum_res._rslt, a);
        if (!(sum_res._valid)) return result(ERROR_NUM, false);
    }
    return sum_res;	
}

result  pow(int a, int b) {
    if (a == ERROR_NUM || b == ERROR_NUM) return result(ERROR_NUM, false);

	
  int exponent = 1;
  for(int i = 0; i < b; i++) {
  	result mul_res = multiply(exponent, a);
    if (!(mul_res._valid)) return result(ERROR_NUM, false);
    exponent = mul_res._rslt;
  }

  if (exponent == ERROR_NUM) return result(ERROR_NUM, false);
  return result(exponent, true);
}

//int main() {
//    result res = multiply(8200, 2);
//    if (!(res._valid))
//        std::cout << "[!] 8200 not allowed" << std::endl;
//    std::cout << res._valid << std::endl;
//    std::cout << res._rslt << std::endl;
//
//}