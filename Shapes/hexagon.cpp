#include "hexagon.h"

hexagon::hexagon(std::string name, std::string color, double side) : Shape(name, color)
{
	set_side(side);
}

void hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor()
		<< std::endl << "Name is " << getName() << std::endl
		<< "Side length is " << get_side() << std::endl
		<< "Area is: " << CalArea() << std::endl;;
}

double hexagon::CalArea()
{
	return MathUtils::cal_hexagon_area(this->_side);
}

void hexagon::set_side(double side)
{
	if (side < 0) throw shapeException();
	this->_side = side;
}

double hexagon::get_side()
{
	return this->_side;
}
