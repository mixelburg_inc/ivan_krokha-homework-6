#include "pentagon.h"

pentagon::pentagon(std::string name, std::string color, double side) : Shape(name, color)
{
	set_side(side);
}

void pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor()
			  << std::endl << "Name is " << getName() << std::endl
			  << "Side length is " << get_side() << std::endl
			  << "Area is: " << CalArea() << std::endl;;
}

double pentagon::CalArea()
{
	return MathUtils::cal_pentagon_area(this->_side);
}

void pentagon::set_side(double side)
{
	if (side < 0) throw shapeException();
	this->_side = side;
}

double pentagon::get_side()
{
	return this->_side;
}
