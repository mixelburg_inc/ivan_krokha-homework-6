#pragma once
#include <exception>

struct shapeException : public std::exception
{
	virtual const char* what() const
	{
		return "[!] This is a shape exception!";
	}
};

struct inputException : public std::exception
{
	virtual const char* what() const
	{
		return "[!] This is an input exception!";
	}
};