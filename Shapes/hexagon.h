#pragma once

#include "exceptions.h"
#include "shape.h"
#include "MathUtils.h"

class hexagon : public Shape
{
public:
	hexagon(std::string name, std::string color, double side);
	void draw();
	double CalArea() override;
	void set_side(double side);
	double get_side();
private:
	double _side;
};
