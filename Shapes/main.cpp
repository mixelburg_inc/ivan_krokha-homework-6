#include <iostream>
#include <string>

#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "pentagon.h"
#include "hexagon.h"

#include "exceptions.h"
#include "addons.h"

const int MIN_ANGLE = 0;
const int MAX_ANGLE = 180;

//
//
//
// simple creator functions 
// just because main was so ugly, I couldn't work with it
// seriously, you, magshimim programmers should write better (cleaner) code
// because, what you did - isn't a really good expample for the students
//
//
//
void create_circle() {
	std::cout << "enter color, name,  rad for circle" << std::endl;
	std::string col = addons::get_string(MSG[enter_color]);
	std::string nam = addons::get_string(MSG[enter_name]);
	int	rad = addons::get_num(MSG[enter_radius]);

	Circle circ(col, nam, rad);
	circ.draw();
}

void create_quad() {
	std::cout << "enter name, color, height, width" << std::endl;
	std::string col = addons::get_string(MSG[enter_color]);
	std::string nam = addons::get_string(MSG[enter_name]);
	int height = addons::get_num(MSG[enter_height]);
	int width = addons::get_num(MSG[enter_width]);

	quadrilateral quad(nam, col, width, height);
	quad.draw();
}

void create_rectangle() {
	std::cout << "enter name, color, height, width" << std::endl;
	std::string col = addons::get_string(MSG[enter_color]);
	std::string nam = addons::get_string(MSG[enter_name]);
	int height = addons::get_num(MSG[enter_height]);
	int width = addons::get_num(MSG[enter_width]);

	rectangle rec(nam, col, width, height);
	rec.draw();
}

void create_parallelogram() {
	std::cout << "enter name, color, height, width, 2 angles" << std::endl;
	std::string col = addons::get_string(MSG[enter_color]);
	std::string nam = addons::get_string(MSG[enter_name]);
	int height = addons::get_num(MSG[enter_height]);
	int	width = addons::get_num(MSG[enter_width]);
	int ang = addons::get_num(MSG[enter_angle], MIN_ANGLE, MAX_ANGLE);
	int ang2 = addons::get_num(MSG[enter_angle], MIN_ANGLE, MAX_ANGLE);

	parallelogram para(nam, col, width, height, ang, ang2);
	para.draw();
}

void create_pentagon() {
	std::cout << "enter name, color, side" << std::endl;
	std::string col = addons::get_string(MSG[enter_color]);
	std::string nam = addons::get_string(MSG[enter_name]);
	int side = addons::get_num(MSG[enter_side]);
	
	pentagon penta(nam, col, side);
	penta.draw();
}

void create_hexagon() {
	std::cout << "enter name, color, side" << std::endl;
	std::string col = addons::get_string(MSG[enter_color]);
	std::string nam = addons::get_string(MSG[enter_name]);
	int side = addons::get_num(MSG[enter_side]);

	hexagon hexa(nam, col, side);
	hexa.draw();
}

int main()
{
	//std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	//Circle circ(col, nam, rad);
	//quadrilateral quad(nam, col, width, height);
	//rectangle rec(nam, col, width, height);
	//parallelogram para(nam, col, width, height, ang, ang2);

	//Shape *ptrcirc = &circ;
	//Shape *ptrquad = &quad;
	//Shape *ptrrec = &rec;
	//Shape *ptrpara = &para;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle_c = 'c', quadrilateral_c = 'q', rectangle_c = 'r', parallelogram_c = 'p', pentagon_c = 'n', hexagon_c = 'h'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, n = pentagon, h = hexagon" << std::endl;
		shapetype = addons::get_char(MSG[enter_choice]);
		try
		{

			switch (shapetype) {
			case circle_c:
				create_circle();
				//std::cout << "enter color, name,  rad for circle" << std::endl;
				//col = addons::get_string(MSG[enter_color]);
				//nam = addons::get_string(MSG[enter_name]);
				//rad = addons::get_num(MSG[enter_radius]);
				//circ.setColor(col);
				//circ.setName(nam);
				//circ.setRad(rad);
				//ptrcirc->draw();
				break;
			case quadrilateral_c:
				create_quad();
				//std::cout << "enter name, color, height, width" << std::endl;
				//col = addons::get_string(MSG[enter_color]);
				//nam = addons::get_string(MSG[enter_name]);
				//height = addons::get_num(MSG[enter_height]);
				//width = addons::get_num(MSG[enter_width]);
				//quad.setName(nam);
				//quad.setColor(col);
				//quad.setHeight(height);
				//quad.setWidth(width);
				//ptrquad->draw();
				break;
			case rectangle_c:
				create_rectangle();
			/*	std::cout << "enter name, color, height, width" << std::endl;
				col = addons::get_string(MSG[enter_color]);
				nam = addons::get_string(MSG[enter_name]);
				height = addons::get_num(MSG[enter_height]);
				width = addons::get_num(MSG[enter_width]);
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();*/
				break;
			case parallelogram_c:
				create_parallelogram();
			/*	std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				col = addons::get_string(MSG[enter_color]);
				nam = addons::get_string(MSG[enter_name]);
				height = addons::get_num(MSG[enter_height]);
				width = addons::get_num(MSG[enter_width]);
				ang = addons::get_num(MSG[enter_angle], MIN_ANGLE, MAX_ANGLE);
				ang2 = addons::get_num(MSG[enter_angle], MIN_ANGLE, MAX_ANGLE);
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();*/
				break;
			case pentagon_c:
				create_pentagon();
				break;
			case hexagon_c:
				create_hexagon();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			x = addons::get_char(MSG[enter_choice]);
		}
		catch (shapeException& e) 
		{
			std::cout << e.what() << std::endl;
		}
		catch (std::exception& e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}
	system("pause");
	return 0;
}