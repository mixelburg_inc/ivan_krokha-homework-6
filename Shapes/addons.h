#pragma once

#include <iostream>
#include <map>

struct addons
{
	/**
	 * @brief checks if string is integer number
	 * @param s string
	*/
	static void is_number(const std::string& s);

	/**
	* @brief converts string to int
	* @param str string
	* @return int from string
	*/
	static int to_int(const std::string& str);

	/**
	 * @brief gets integer number from user
	* @param message - message to be printed to user
	* @param min_value - minimal valid value
	 * @param max_value - maximal valid value
	* @return integer number from user
	*/
	static int get_num(std::string& message, int min_value = INT_MIN, int max_value = INT_MAX);

	/**
	* @brief gets string from user
	* @param message - message to be printed to user
	* @return string input from user
	*/
	static std::string get_string(std::string& message);

	/**
	 * @brief gets char from user
	 * @param message - message to be printed to user 
	 * @return char input from user
	*/
	static char get_char(std::string& message);
};

// text id's
static enum msgs
{
	// info messages
	enter_radius,
	enter_name,
	enter_width,
	enter_length,
	enter_height,
	enter_side,
	enter_color,
	enter_num_points,
	enter_angle,
	enter_choice,

	// exit message
	exit_msg,
};
// map with texts
static std::map<msgs, std::string> MSG = {
	// info messages
	{enter_radius, "enter radius: "},
	{enter_name, "enter name: "},
	{enter_width, "enter width: "},
	{enter_length, "enter length: "},
	{enter_height, "enter height: "},
	{enter_side, "enter side: "},
	{enter_color, "enter color: "},
	{enter_angle, "enter angle: "},
	{enter_choice, "enter choice: "},

	// exit message
	{exit_msg, "[+] exiting..."},
};
