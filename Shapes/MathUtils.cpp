#include "MathUtils.h"

double MathUtils::cal_pentagon_area(double& side)
{
	if (side < 0) throw shapeException();
	return 0.25 * sqrt(5 * (5 + 2 * sqrt(5))) * pow(side, 2);
}

double MathUtils::cal_hexagon_area(double& side)
{
	if (side < 0) throw shapeException();
	return (3 * sqrt(3) / 2) * pow(side, 2);
}
